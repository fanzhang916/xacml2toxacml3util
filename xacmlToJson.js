const fs = require('fs');
const xml2js = require('xml2js');
const parseXML = xml2js.parseString;

// const dir = './sample/multi22-xacml3'
// const xacmlJsonDir = './sample/multi22-json'

const dir = './sample/single-xacml3'
const xacmlJsonDir = './sample/single-json'

const attributeCategories = {
  'urn:oasis:names:tc:xacml:3.0:attribute-category:resource' : 'Resource',
  'urn:oasis:names:tc:xacml:3.0:attribute-category:action' : 'Action',
  'urn:oasis:names:tc:xacml:3.0:attribute-category:environment' : 'Environment',
  'urn:oasis:names:tc:xacml:1.0:subject-category:access-subject' : 'AccessSubject',
  'urn:oasis:names:tc:xacml:1.0:subject-category:recipient-subject' : 'RecipientSubject',
  'urn:oasis:names:tc:xacml:1.0:subject-category:intermediary-subject' : 'IntermediarySubject',
  'urn:oasis:names:tc:xacml:1.0:subject-category:codebase' : 'Codebase',
  'urn:oasis:names:tc:xacml:1.0:subject-category:requesting-machine' : 'RequestingMachine'
}
const parseXacml3ToJson = ({Request}) => {
  const RequestObj = {
    ...Request.$,
  }

  Request.Attributes.forEach(attribute => {
    const categoryName = attributeCategories[attribute.$.Category]
    RequestObj[categoryName] = { "Attribute" : []}
    if (attribute.Attribute) {
      const Attribute = attribute.Attribute.map(attributeValue => {
        return {
          ...attributeValue.$,
          Value: attributeValue.AttributeValue[0]._,
          DataType: attributeValue.AttributeValue[0].$.DataType
        }
      })
      RequestObj[categoryName].Attribute = Attribute
    }

  })
  return JSON.stringify(RequestObj, null, 2)
}


fs.readdir(dir, (err, files) => {
  files.forEach(file => {
      fs.readFile(`${dir}/${file}`, (err, data) => {
          parseXML(data, (err, result) => {
            const parsedJsonValue = parseXacml3ToJson(result)
              fs.writeFile(`${xacmlJsonDir}/${file.replace('xml', 'json')}`, parsedJsonValue, (err) => {
                  if (err) throw err;

                  console.log('XACML3 saved!');
              });
          });
      });
  })
})
