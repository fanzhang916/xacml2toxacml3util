const fs = require('fs');
const xml2js = require('xml2js');
const parseXML = xml2js.parseString;
const XACML3builder = new xml2js.Builder();

const dir = './sample/multi22'
const xacml3Dir = './sample/multi22-xacml3'

const abstractAttr = (attrs) => {
    return attrs.Attribute.map(attr => {
        return {
            $: {
                AttributeId: attr.$.AttributeId,
                IncludeInResult: false
            },
            AttributeValue: [{
                $: {
                    DataType: attr.$.DataType
                },
                _: attr.AttributeValue
            }]

        }
    })
}

const getAllAttributes = (xacml2) => {
    const attributes = {
        Request: {
            $: {
                'xsi:schemaLocation': "urn:oasis:names:tc:xacml:3.0:core:schema:wd-17 xacml-core-v3-schema-wd-17.xsd",
                xmlns: "urn:oasis:names:tc:xacml:3.0:core:schema:wd-17",
                'xmlns:xsi': "http://www.w3.org/2001/XMLSchema-instance",
                ReturnPolicyIdList: false,
                CombinedDecision: false
            },
            Attributes: []
        }
    };

    xacml2.Request.Subject.forEach(subject => {
        const Attributes = {
            $: {
                "Category": "urn:oasis:names:tc:xacml:1.0:subject-category:access-subject"
            },
            Attribute: []
        };

        Attributes.Attribute.push(...abstractAttr(subject));
        attributes.Request.Attributes.push(Attributes);
    });

    xacml2.Request.Resource.forEach(subject => {
        const Attributes = {
            $: {
                "Category": "urn:oasis:names:tc:xacml:3.0:attribute-category:resource"
            },
            Attribute: []
        };

        Attributes.Attribute.push(...abstractAttr(subject));
        attributes.Request.Attributes.push(Attributes);
    })

    xacml2.Request.Action.forEach(subject => {
        const Attributes = {
            $: {
                "Category": "urn:oasis:names:tc:xacml:3.0:attribute-category:action"
            },
            Attribute: []
        };

        Attributes.Attribute.push(...abstractAttr(subject));
        attributes.Request.Attributes.push(Attributes);
    })

    if (xacml2.Request.Environment[0] === "") {
        attributes.Request.Attributes.push({
            $: {
                "Category": "urn:oasis:names:tc:xacml:3.0:attribute-category:environment"
            },
        });
    } else {
        xacml2.Request.Environment.forEach(subject => {
            const Attributes = {
                $: {
                    "Category": "urn:oasis:names:tc:xacml:1.0:subject-category:access-subject"
                },
                Attribute: []
            };

            Attributes.Attribute.push(...abstractAttr(subject));
            attributes.Request.Attributes.push(Attributes);
        })
    }


    return XACML3builder.buildObject(attributes);
}

// fs.readdir(dir, (err, files) => {
//     files.forEach(file => {
//         fs.readFile(`${dir}/${file}`, (err, data) => {
//             parseXML(data, (err, result) => {
//                 const xacml3 = getAllAttributes(result)
//                 fs.writeFile(`${xacml3Dir}/${file}`, xacml3, (err) => {
//                     if (err) throw err;

//                     console.log('XACML3 saved!');
//                 });
//             });
//         });
//     })
// })

fs.readdir(dir, (err, files) => {
  files.forEach(file => {
      fs.readFile(`${dir}/${file}`, (err, data) => {
          parseXML(data, (err, result) => {
              const xacml3 = getAllAttributes(result)
              xml2js.parseString(xacml3, function (err, result) {
                const json = JSON.stringify(result);

                  console.log(json);
                  throw new Error('')
              })
              // fs.writeFile(`${xacml3Dir}/${file}`, xacml3, (err) => {
              //     if (err) throw err;

              //     console.log('XACML3 saved!');
              // });
          });
      });
  })
})